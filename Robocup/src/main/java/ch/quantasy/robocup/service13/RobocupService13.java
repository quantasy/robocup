/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.robocup.service13;

import ch.quantasy.robocup.service14.*;
import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import ch.quantasy.robocup.service04.MillisecondsEvent;
import ch.quantasy.robocup.service04.RobocupService04YAMLContract;
import java.net.URI;
import java.util.Set;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author reto
 */
public class RobocupService13 {
    
    
    private MQTTGatewayClient<RobocupService04YAMLContract> mqc;
    
    public RobocupService13(URI mqttURI) throws MqttException{
        mqc=new MQTTGatewayClient(mqttURI, "RobocupService13ClientID", new RobocupService04YAMLContract("Robocup","Service","03"));
        mqc.connect();
        System.out.println(mqc.getContract().EVENT_MILLISECONDS);
        mqc.subscribe(mqc.getContract().EVENT_MILLISECONDS, (topic, payload) -> {
            String hoffentlichIstEsEinString=new String(payload);
                System.out.println(hoffentlichIstEsEinString);
        });
    }
    
    public static void main(String[] args) throws Exception{
        URI mqttURI = URI.create("tcp://127.0.0.1:1883");
        if (args.length > 0) {
            mqttURI = URI.create(args[0]);
        } else {
            System.out.printf("Per default, 'tcp://127.0.0.1:1883' is chosen.\nYou can provide another address as first argument i.e.: tcp://iot.eclipse.org:1883\n");
        }
        System.out.printf("\n%s will be used as broker address.\n", mqttURI);
        RobocupService13 robocopService = new RobocupService13(mqttURI);
        System.in.read();
    }
}
