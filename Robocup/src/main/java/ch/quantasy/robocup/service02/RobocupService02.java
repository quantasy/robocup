/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.robocup.service02;

import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import java.net.URI;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author reto
 */
public class RobocupService02 extends MQTTGatewayClient<RobocupService02YAMLContract> {

    public RobocupService02(URI mqttURI) throws MqttException {
        super(mqttURI, "RobocupService02ClientID", new RobocupService02YAMLContract("Robocup", "Service", "02"));
        super.connect();
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
        executor.scheduleAtFixedRate(() -> {
            super.readyToPublish(super.getContract().EVENT + "/" + "ein beliebiges Event", new String("" + System.currentTimeMillis()).getBytes());
        }, 0, 500, TimeUnit.MILLISECONDS);
        super.readyToPublish(super.getContract().EVENT_MILLISECONDS, new String("" + System.currentTimeMillis()).getBytes());
    }

    public static void main(String[] args) throws Exception {
        URI mqttURI = URI.create("tcp://127.0.0.1:1883");
        if (args.length > 0) {
            mqttURI = URI.create(args[0]);
        } else {
            System.out.printf("Per default, 'tcp://127.0.0.1:1883' is chosen.\nYou can provide another address as first argument i.e.: tcp://iot.eclipse.org:1883\n");
        }
        System.out.printf("\n%s will be used as broker address.\n", mqttURI);
        RobocupService02 control = new RobocupService02(mqttURI);
        System.in.read();
    }

}
