/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.robocup.service02;

import ch.quantasy.mdservice.message.Message;
import ch.quantasy.mdsmqtt.gateway.client.contract.AyamlServiceContract;
import java.util.Map;

/**
 *
 * @author reto
 */
public class RobocupService02YAMLContract extends AyamlServiceContract {
    public static String EVENT_MILLISECONDS;
    public RobocupService02YAMLContract(String rootContext, String baseClass, String instance) {
        super(rootContext, baseClass, instance);
        EVENT_MILLISECONDS=EVENT+"/"+"Milliseconds";
    }

    @Override
    public void setMessageTopics(Map<String, Class<? extends Message>> messageTopicMap) {
        //Das wird momentan 'nur' gebraucht, wenn Du die Topics beschreiben möchtest...
    }

}
