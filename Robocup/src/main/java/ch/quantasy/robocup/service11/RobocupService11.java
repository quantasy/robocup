/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.robocup.service11;

import ch.quantasy.mdsmqtt.gateway.client.MQTTGatewayClient;
import ch.quantasy.robocup.service01.RobocupService01YAMLContract;
import java.net.URI;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author reto
 */
public class RobocupService11 {
    
    
    private MQTTGatewayClient<RobocupService01YAMLContract> mqc;
    
    public RobocupService11(URI mqttURI) throws MqttException{
        mqc=new MQTTGatewayClient(mqttURI, "RobocupService11ClientID", new RobocupService01YAMLContract("Robocup","Service","01"));
        mqc.connect();
        System.out.println(mqc.getContract().EVENT_MILLISECONDS);
        mqc.subscribe("EinBeliebigesTopic", (topic, payload) -> {
            String hoffentlichIstEsEinString=new String(payload);
                System.out.println(hoffentlichIstEsEinString);
            
        });
    }
    
    public static void main(String[] args) throws Exception{
        URI mqttURI = URI.create("tcp://127.0.0.1:1883");
        if (args.length > 0) {
            mqttURI = URI.create(args[0]);
        } else {
            System.out.printf("Per default, 'tcp://127.0.0.1:1883' is chosen.\nYou can provide another address as first argument i.e.: tcp://iot.eclipse.org:1883\n");
        }
        System.out.printf("\n%s will be used as broker address.\n", mqttURI);
        RobocupService11 robocopService = new RobocupService11(mqttURI);
        System.in.read();
    }
}
