/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.robocup.service04;

import ch.quantasy.mdservice.message.AnEvent;
import ch.quantasy.mdservice.message.annotations.NonNull;
import ch.quantasy.mdservice.message.annotations.Period;

/**
 *
 * @author reto
 */
public class MillisecondsEvent extends AnEvent{
    @NonNull
    @Period
    public Long value;

    private MillisecondsEvent(){
        
    }
    
    public MillisecondsEvent(Long value) {
        this.value = value;
    }
    
    
}
